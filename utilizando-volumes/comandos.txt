# docker volume create meu-volume
- cria volume

# docker run -it -v meu-volume: /app ubuntu bash
- cria container ubuntu, com o meu-volume mapeado como /app dentro do container

# /var/lib/docker
- local onde ficam os volumes

# docker volume
- mostra todos os comandos

# docker run -it --mount source=meu-volume,target=/app ubuntu bash
- metodo recomendado
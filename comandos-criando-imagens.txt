# FROM
- cria imagem apartir de uma camada (ex= FROM node:14)

# COPY
- copia dados de um endereço do host, para um endereço dentro da imagem (ex= COPY . /app-na-imagem)

# RUN
- executa comandos (ex= npm install)

# ENTRYPOINT 
- comando que inicia a imagem (ex= npm start)

# WORKDIR
- define diretorio de trabalho dentro da imagem (ex= /app-node)

# docker build -t nomedaimagem:tag .
- faz a build da imagem apartir do arquivo Dockerfile no diretorio do arquivo

# EXPOSE
- explicita em qual porta roda a aplicação

# ARG
- cria variavel de ambiente, mas somente na fase de criação da imagem

# ENV
- cria variavel de ambiente em tempo de execução da imagem

